import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './modules/components/home/home.component';
import { FeatureComponent } from './modules/components/feature/feature.component';
import { AboutComponent } from './modules/components/about/about.component';
import { NavbarComponent } from './modules/components/navbar/navbar.component';
import { OurTeamComponent } from './modules/components/our-team/our-team.component';
import { ReviewsComponent } from './modules/components/reviews/reviews.component';
import { SupportComponent } from './modules/components/support/support.component';
import { FooterComponent } from './modules/components/footer/footer.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'feature', component: FeatureComponent},
  {path:'about', component: AboutComponent},
  {path: 'navbar', component: NavbarComponent},
  {path: 'our-team', component: OurTeamComponent},
  {path: 'reviews', component: ReviewsComponent},
  {path: 'support', component: SupportComponent},
  {path: 'footer', component: FooterComponent},
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
